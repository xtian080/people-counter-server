require('dotenv').config({ path: '.env' })

const { NODE_ENV, APP_PORT } = process.env

const server = require("http").createServer(),
  mqtt = require('mqtt'),
  io = require("socket.io")(server, {
    cors: { origin: "*", },
  })

const e = require('express')
const Count = require('./app/controller/Count')

const PORT = 5009
const WS_EVENT = "newEvent"
const host = '192.168.1.26'
const port = '1883'
const clientId = `mqtt_${Math.random().toString(16).slice(3)}`

const connectUrl = `mqtt://${host}:${port}`
const mqttClient = mqtt.connect(connectUrl, {
  clientId,
  clean: true,
  connectTimeout: 4000,
  username: 'conversar',
  password: '0',
  reconnectPeriod: 1000,
})

const topic = 'pc/entrance/live_counts'

mqttClient.on('connect', () => {
  mqttClient.subscribe([topic], () => {
    console.log(`Connected: Subscribe to topic '${topic}'`)
  })
})

mqttClient.on('message', async (topic, payload) => {
  let formattedCounts = {}, peopleEntered = 0
  const { counts, time, index } = JSON.parse(payload.toString())

  counts.forEach(({ name, count }) => {
    formattedCounts[name] = count
  })

  
  let countNow = await Count.index(formattedCounts)
  
  console.log(countNow)
  
  let isZero = countNow <= 0
  
  Count.save(formattedCounts, isZero)

  peopleEntered = isZero ? 0 : countNow

  io.in('peopleCounter').emit(WS_EVENT, {
    body: typeof peopleEntered === 'number' ? peopleEntered : 0
  })
})

io.on("connection", (socket) => {
  const { query: { eventName } } = socket.handshake

  socket.join(eventName)

  socket.on(WS_EVENT, (data) => {
    checkData(data)
    io.in(eventName).emit(WS_EVENT, data)
  })

  socket.on("disconnect", (eventName) => {
    socket.leave(eventName)
    console.log('Disconnected: ', eventName)
  })
})

const checkData = (data) => {
  console.log(data.body)
  switch (data.body) {
    case 'resetCount':
      return Count.reset()
    default:
      return 0
  }
}

server.listen(PORT, () => {
  console.log(`LoP: ${PORT}`)
})