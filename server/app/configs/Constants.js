const LOG_DIR = './app/logs'
const SAVE_COUNT_DATA = './app/data/json/lastSaveCountData.json'
const RESET_COUNT_DATA = './app/data/json/lastResetCountData.json'

module.exports = { LOG_DIR, SAVE_COUNT_DATA, RESET_COUNT_DATA }