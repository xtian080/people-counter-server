const moment = require('moment')
const { createLogger, format, transports } = require('winston')

const { combine, timestamp, label, prettyPrint } = format

const yearToday = moment().format('yyyy')
  dayMonthToday = moment().format('MM/DD')

exports.myTrail = async (level, message, labelText = 'System log', folderName = 'server') => {
  const logger = await createLogger({
    format: combine(
      label({ label: labelText }),
      timestamp(),
      prettyPrint()
    ),
    transports: [
      new transports.Console(),
      new transports.File({ filename: `./app/logs/${yearToday + folderName + dayMonthToday}.log` })
    ]
  })

  return logger.log({
    level: level,
    message: message
  })
}