const fs = require('fs')
const { myTrail } = require('../helpers/LogsHelper')
const { SAVE_COUNT_DATA, RESET_COUNT_DATA } = require('../configs/Constants')

const index = (newData = {}) => {
  if (fs.existsSync(RESET_COUNT_DATA)) {
    let resetData = JSON.parse(fs.readFileSync(RESET_COUNT_DATA))
    console.log(resetData, newData)
    let entrance = newData['entrance-door-line'] - resetData['entrance-door-line']
    let exit = newData['exit-door-line'] - resetData['exit-door-line']

    return entrance - exit
  } else return newData['entrance-door-line'] - newData['exit-door-line']
}

const save = (data = {}, isReset = false) => {
  if(typeof data === 'object') fs.writeFile(SAVE_COUNT_DATA, JSON.stringify(data), function (err) {
    if (err) throw err

    if(isReset) reset()
    
    return myTrail('info', `Count - save.`)
  })
}

const reset = () => {
  if (fs.existsSync(SAVE_COUNT_DATA)) {
    let data = JSON.parse(fs.readFileSync(SAVE_COUNT_DATA))
    console.log(data)

    if(typeof data === 'object') fs.writeFile(RESET_COUNT_DATA, JSON.stringify(data), function (err) {
      if (err) throw err
      
      return myTrail('info', `Count - reset.`)
    })
  }
}

module.exports = { save, reset, index }