build:
	cd server && $(MAKE) build
	cd client && $(MAKE) build

run:
	cd server && $(MAKE) run
	cd client && $(MAKE) run

stop:
	docker-compose down