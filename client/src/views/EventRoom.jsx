import React, { useState } from "react"
import useEvent from "../hooks/useEventHooks"
import { Link } from "react-router-dom"
import { FaHome } from 'react-icons/fa'
import { AiOutlineSend } from 'react-icons/ai'

import "./styles/EventRoom.css"

const EventRoom = (props) => {
  const { eventName } = props.match.params
  const { events, sendEvents } = useEvent(eventName)
  const [newEvent, setNewEvent] = useState("")

  const handleNewMessageChange = (e) => {
    setNewEvent(e.target.value)
  }

  const handleSendEvent = () => {
    sendEvents(newEvent)
    setNewEvent("")
  }

  const handleKeyPress = ({ key }) => {
    if (key === "Enter") {
      sendEvents(newEvent)
      setNewEvent("")
    }
  }

  return (
    <div className="events">
      <Link to="/">
        <button className="back"><FaHome /></button>
      </Link>
      <div className="top">
        <span className="name">Event: {eventName}</span>
        <div className="list">
          <ul>
            {events.map((event, i) => (
              <li key={i} className={`${event.ownedByCurrentUser ? "sent" : "received"}`}>
                <div className={`event text ${event.ownedByCurrentUser ? "sent" : "received"}`}>
                  {event.body}
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
      <div className="bottom">
        <input
          value={newEvent}
          onKeyPress={handleKeyPress}
          onChange={handleNewMessageChange}
          placeholder="Add event..."
          className="add-event"
        />
        <button onClick={handleSendEvent} className="send"><AiOutlineSend /></button>
      </div>
    </div>
  )
}

export default EventRoom