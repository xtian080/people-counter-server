import React, { useState } from "react"
import { Link } from "react-router-dom"
import ReactPlayer from 'react-player'
import bg from 'assets/videos/bg.mp4'

import "./styles/Home.css"

const Home = () => {
  const [eventName, setRoomName] = useState("peopleCounter")

  const handleEventNameChange = (event) => {
    setRoomName(event.target.value)
  }

  return (
    <div className="home container">
      <ReactPlayer
        className="bg-video"
        url={bg}
        playing
        loop
        muted
      />
      <span className="label title">Event Controller</span>
      <input
        type="text"
        placeholder="Event name"
        value={eventName}
        onChange={handleEventNameChange}
        className="event"
      />
      <Link to={`/event/${eventName}`}>
        <button className="enter-link">Join</button>
      </Link>
    </div>
  )
}

export default Home