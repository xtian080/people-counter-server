import { useEffect, useRef, useState } from "react"
import socketIOClient from "socket.io-client"

const WS_EVENT = "newEvent" // Name of the event
const SOCKET_SERVER_URL = "http://localhost:5009"

const useEventHooks = (eventName) => {
  const [events, setEvents] = useState([]) // Sent and received events
  const socketRef = useRef()

  useEffect(() => {
    
    // Creates a WebSocket connection
    socketRef.current = socketIOClient(SOCKET_SERVER_URL, {
      query: { eventName },
    })
    
    // Listens for incoming events
    socketRef.current.on(WS_EVENT, (events) => {
      const incomingMessage = {
        ...events,
        ownedByCurrentUser: events.senderId === socketRef.current.id,
      }
      setEvents((events) => [...events, incomingMessage])
    })
    
    // Destroys the socket reference
    // when the connection is closed
    return () => {
      socketRef.current.disconnect()
    }
  }, [eventName])

  // Sends a events to the server that
  // forwards it to all users in the same room
  const sendEvents = (messageBody) => {
    socketRef.current.emit(WS_EVENT, {
      body: messageBody,
      senderId: socketRef.current.id,
    })
  }

  return { events, sendEvents }
}

export default useEventHooks