import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

import Home from "./views/Home"
import EventRoom from "./views/EventRoom"

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/event/:eventName" component={EventRoom} />
      </Switch>
    </Router>
  )
}

export default App